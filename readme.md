# Bienvenido to movie search
## Pasos para correr buscador de películas 

El requisito base es crear un archivo .env en el root de la repo
	
    API_KEY=4fdcef3
    
Lugo ejecutar en la linea de comandos en el root de la repo para instalar las dependencias

    npm install 

## Ejecución 
En el root del proyecto ejecutar

    npm run dev

Luego abrir el en el navegador el puerto por defecto :3000

[http://localhost:3000](http://localhost:3000)

[http://localhost:3000/favorites](http://localhost:3000/favorites)
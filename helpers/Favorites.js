export default class Favorites{
    constructor() {
        let _favorites = localStorage.getItem('favorites')
        if(_favorites==undefined){
            _favorites = []
            localStorage.setItem('favorites', JSON.stringify([]));
        }else{
            _favorites = JSON.parse(_favorites)
        }
        this.fav = _favorites
    }
    updateFavorites(movie){
        if(this._exist(movie.imdbID)){
            const index = this.fav.indexOf(movie)
            this.fav.splice(index,1)
        }else{
            this.fav.push(movie)
        }
        this._save()
    }
    is_favorite(movie){
        this._exist(movie.imdbID) ? true:false
    }
    getFavorites(){
        return this.fav
    }
    _exist(movieId){
        try{
            return this.fav.find(movie => movie.imdbID == movieId)
        }catch (error) {
            return false
        }
    }
    _save(){
        console.log(this.fav)
        localStorage.setItem('favorites',  JSON.stringify(this.fav));
    }
}
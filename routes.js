const routes = require('next-routes')

module.exports = routes()
.add('index')
.add('movie', '/movie/:slug.:id','movie')
.add('favorites','/favorites')

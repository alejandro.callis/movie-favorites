import Head from 'next/head'
import Link from 'next/link'

import {AppBar,Grid,Toolbar,Typography} from '@material-ui/core'


export default class Layaut extends React.Component {
    render() {
        const classes = {
            root: {
                flexGrow: 1,
            },
            menuButton: {
                marginRight: 0,
            },
            title: {
                flexGrow: 1,
            },
            titleButton:{
                backgroundColor: 'transparent'
            }
        }
        const { children, title } = this.props
        return (
            <div>
                <Head>
                    <meta name="viewport" content="width=device-width, user-scalable=no" />
                    <title>{title}</title>
                    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
                </Head>
                <header>
                    <AppBar position="static">
                        <Toolbar>
                            <Typography variant="body2" className={classes.title}>
                                MOVIES
                            </Typography>
                        </Toolbar>
                    </AppBar>

                </header>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    spacing={5}
                >
                    <Grid container item xs={8} >
                        {children}
                    </Grid>
                </Grid>
            </div>
        )
    }
}


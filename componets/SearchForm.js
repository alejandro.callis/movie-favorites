import 'isomorphic-fetch'
import Error from 'next/error'
import { Button, TextField, Typography, Grid } from '@material-ui/core';
import MovieGrid from '../componets/MovieGrid'



const API_KEY = process.env.API_KEY

export default class SearchForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            title: '',
            movies: []

        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const title = event.target.value
        this.setState({ title: title })
    }

    async handleSubmit(event) {
        event.preventDefault()
        try {
            let request = await fetch(`http://www.omdbapi.com/?apikey=${API_KEY}&s=${this.state.title}`)
            let { Search } = await request.json()
            this.setState({ movies: Search })
        } catch (error) {
            console.log({ movies: null, statusCode: 404 })
        }
    }

    render() {
        return (
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
            >
                <div>
                    <div>
                        <form onSubmit={this.handleSubmit}>
                            <TextField id="filled-basic" onChange={this.handleChange} label="Search for a title" variant="filled" />
                            <Button type="submit" variant="contained" color="primary">Search</Button>
                        </form>
                    </div>
                    <div>

                        {this.state.movies ? <MovieGrid movies={this.state.movies} /> : false}

                    </div>
                </div>
            </Grid>
        )
    }
}

/*<MovieGrid movies={Search} /> */



import {Link} from '../routes'
import slug from '../helpers/slug'


export default class MovieGrid extends React.Component {
    render() {
        const { movies } = this.props
        return (
            <div>
                <div className='movies'>
                    {movies ? false:"No encontrado"}
                    {movies.map((movie) => {
                        const { imdbID, Title, Year,Poster } = movie
                        return (
                            <Link route='movie' params={{
                                id: imdbID,
                                slug: slug(Title)
                                }}>
                                <a >
                                    <div className="movie" key={imdbID} >
                                        <img src={Poster} alt={Title} />
                                        <h2>{Title}</h2>
                                        <p>Year: {Year}</p>
                                    </div>
                                </a>
                            </Link>
                        )
                    })}
                </div>
                <style jsx>{`
                :global(body){
                padding: 0;
                margin: 0;
                }
                header {
                background-color: #1d76db;
                color: white;
                padding: 15px;
                text-align: center;
                }
                .movies {
                display: grid;
                grid-gap: 15px;
                padding: 15px;
                grid-template-columns: repeat(auto-fill, minmax(160px, 1fr));
                }
                a.movie {
                display: block;
                margin-bottom: 0.5em;
                color: #333;
                text-decoration: none;
                }
                .movie img {
                border-radius: 3px;
                box-shadow: 0px 2px 6px rgba(0,0,0,0.15);
                width: 100%;
                }
                h2 {
                padding: 5px;
                font-size: 0.9em;
                font-weight: 600;
                margin: 0;
                text-align: center;
                }
            `}</style>
            </div>
        )
    }
}
import 'isomorphic-fetch'
import movieStyle from '../style/movie.jsx'
import Layaut from '../componets/Layaut'
import { Grid, IconButton } from '@material-ui/core';

import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import StarIcon from '@material-ui/icons/Star';
import Favorites from '../helpers/Favorites'
const API_KEY = process.env.API_KEY
const favorites = new Favorites()
const useStyles = {
    root: {
        width: '100%',
    }
}


const Movie = ({ movie, statusCode }) => {
    
    const classes = useStyles;
    if (statusCode !== 200) {
        return <Error statusCode={statusCode} />
    }
    function updateFavorites(){
        favorites.updateFavorites(movie)
    }
    return (
        <Layaut title={movie.Title}>
            <Grid container justify="center" alignItems="center">
                <Grid item >
                    <img src={movie.Poster} alt={movie.Title} />
                    <Typography variant="h4">{movie.Title}</Typography>
                    <IconButton color={favorites.is_favorite(movie.imdbID)? "secondary":"primary" } onClick={updateFavorites}>
                        <StarIcon  />
                    </IconButton>
                    <Typography variant="h5">Year {movie.Year}</Typography>
                    <Typography variant="p">{movie.Plot}</Typography>
                    <List component="nav" className={classes.root} aria-label="ratings">

                        {
                            movie.Ratings.map((rating) => {
                                return (
                                    <ListItem button>
                                        <ListItemText primary={`${rating.Source} - ${rating.Value}`} />
                                    </ListItem>
                                )
                            })

                        }
                    </List>

                </Grid>

            </Grid>
        </Layaut >
    )
}
Movie.getInitialProps = async ({ query, res }) => {
    try {
        const idmovie = query.id
        let request = await fetch(`http://www.omdbapi.com/?plot=full&apikey=${API_KEY}&i=${idmovie}`)
        let movie = await request.json()
        return { movie, statusCode: 200 }
    } catch (error) {
        res.statusCode = 404
        return { movie: {}, statusCode: 404 }
    }
}
export default Movie
import 'isomorphic-fetch'
import Layaut from '../componets/Layaut'
import SearchForm from '../componets/SearchForm'
import Typography from '@material-ui/core/Typography';


export default class extends React.Component {
    render() {
        return <Layaut title="Search for your favorite movie">
            <Typography variant="h3" color="textPrimary" align="center">
                Search for your favorites movies
            </Typography>
            <SearchForm />
        </Layaut>
    }
}
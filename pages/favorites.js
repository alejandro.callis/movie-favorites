import 'isomorphic-fetch'
import Layaut from '../componets/Layaut'
import SearchForm from '../componets/SearchForm'
import Typography from '@material-ui/core/Typography';
import Favorites from '../helpers/Favorites'
import MovieGrid from '../componets/MovieGrid'


const Favorite = ({ fav }) => {
    fav = Array.isArray(fav) ? fav:[]
    console.log(fav)
    return (
        <Layaut title="Search for your favorite movie">
            <Typography variant="h3" color="textPrimary" align="center">
                Your favorites
            </Typography>
            <MovieGrid movies={fav} />
        </Layaut>
    )
}

Favorite.getInitialProps = async ({ res }) => {
    try {
        const favorites = new Favorites()
        let fav = favorites.getFavorites()
        return { fav, statusCode: 200 }
    } catch (error) {
        res.statusCode = 404
        return { fav:{}, statusCode: 404 }
    }
}
export default Favorite